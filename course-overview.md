# Мистецтво программування за допомогою Python

## Вступ. 
*Інструменти та засоби:* GitLab, Jupyter, VSCode.

### Заняття 1.
Шаблони программування, методи та засоби для написання чистого коду і налаштування середовища для розробки. Перша программа.

[Конспект](notes/class-1.md)



## Додаткові матеріали:
- [Allen B. Downey "Think Python: How to Think Like a Computer Scientist"](https://greenteapress.com/wp/think-python-2e/)
- [Python Enhancement Proposals](https://peps.python.org)
  - [Style Guide for Python Code](https://peps.python.org/pep-0008/)
- [Python documentation](https://docs.python.org/3/)
- [Jupyter](https://jupyter.org) and [Binder](https://mybinder.org)
- [GitLab](https://docs.gitlab.com/ee/tutorials/index.html) and [Git Cheat Sheet](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet)
